#HOW TO USE THIS CLASS ,  WEB.PY , DATAS
# 1. datas(filename)
# 2. datas.savefile() WILL SAVE A FILE WITH THE FILENAME GIVEN IN 1. IN YOUR CURRENT DIRECTORY
# 3. EITHER extract_past_days FOR ALL PAST 3 DAYS OR extract[today,yesterday,yesterday_two]
# 4. get_[today,yesterday,yesterday_two] TO GET A LIST OF TUPLES
'''
@author: shlomo bensimhon, jamin Huang
'''
from bs4 import BeautifulSoup
import requests

class datas:
    #datas object,path to file where the web content is stored
    #parameter  name = name of the html file to be stored
    def __init__(self,file_name):
        self.file_name = file_name
        self.id=["main_table_countries_today","main_table_countries_yesterday","main_table_countries_yesterday2"]
        self.resultstoday = [] * 220
        self.results_yesterday = [] * 220
        self.results_yesterday_two = [] * 220
    
    #will write to file using filename
    def savefile(self):
        url="https://www.worldometers.info/coronavirus/"
        req=requests.get(url)
        soup=BeautifulSoup(req.text,"html.parser")
        
        file=open(self.file_name,"w", encoding="utf-8")
        file.write(str(soup))
        file.close()

    #start extract days
    def extract_today(self):
        datas._extract(self,self.id[0])
    def extract_yesterday(self):
        datas._extract(self,self.id[1])
    def extract_yesterday_two(self):
        datas._extract(self,self.id[2])

    def extract_past_days(self):
        datas.extract_today(self)
        datas.extract_yesterday(self)
        datas.extract_yesterday_two(self)
    
    def get_today(self):
        return self.resultstoday
    def get_yesterday(self):
        return self.results_yesterday
    def get_yesterday_two(self):
        return self.results_yesterday_two
    

    
    #finds the rows we need
    def _extract(self,day):
        f = open(self.file_name,"r", encoding="utf-8")
        soup = BeautifulSoup(f,"html.parser")
        f.close()

        results = soup.find(id = day)
        results = results.find('tbody')
        allrows = results.find_all('tr',{"style":["","background-color:#EAF7D5","background-color:#F0F0F0"]})

        #print(allrows[220].get_text())
        #221 countries
        datas._arrange(self,allrows,day)
    
    #private method, arranges the rows for our data
    def _arrange(self,allrows,day):
        counter = 0
        for i in allrows:
            s=allrows[counter].get_text()
            sarr=s.split("\n")
            #sarr gives the list of objects of 1 country
            #sarr has a size of 19
        
            for ii in range(3,16):
                sarr[ii]=sarr[ii].replace(',','')
                #replace all commas
            
            ccounter=0
            newarr=[]
            for ii in range(1,17):
                newarr.append(sarr[ii])                
                ccounter += 1
            
            #adds to the object
            if(day == "main_table_countries_today"):
                self.resultstoday.append(tuple(newarr))
            elif(day == "main_table_countries_yesterday"):
                self.results_yesterday.append(tuple(newarr))
            elif(day == "main_table_countries_yesterday2"):
                self.results_yesterday_two.append(tuple(newarr))
            counter += 1