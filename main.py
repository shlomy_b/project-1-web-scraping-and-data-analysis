# -*- coding: utf-8 -*-
"""
Created on Sat Mar 20 22:17:53 2021

@author: shlomo bensimhon, jamin Huang
"""

#Main method
import matplotlib.pyplot as plt
import panda
import database_connection

#show plot for death toll per 1M comparing 5 countries, arr:[countries[],values[]]
def death_toll_plot(arr):
    plt.bar(arr[0],arr[1],label="Death toll",fc="red")
    plt.legend()
    plt.xlabel('countries')
    plt.ylabel('per 1 M')
    plt.title('death toll per country')
    plt.show()

#show plot for total recovered comparing 5 countries
def recovered_plot(arr):
    plt.bar(arr[0],arr[1],label="total recovered",fc="green")
    plt.legend()
    plt.xlabel('countries')
    plt.ylabel('Total recovered')
    plt.title('Total recovered per country')
    plt.show()

#show plot or total tests comparing 5 countries
def total_tests(arr):
    plt.bar(arr[0],arr[2],label="total tests",fc="orange")
    plt.legend()
    plt.xlabel('countries')
    plt.ylabel('Total tests')
    plt.title('Total tests per country')
    plt.show()

#It starts here
#initialization
print("initializing")
try:
    data = database_connection.database_connects()
    data.initialize()
except Exception as e:
            print("Please switch out the mysql database connection values before continuing in panda.py and database_connection.py")
            print("please Rerun the program")
            print(e)
print("ìnitialization complete")

#loops until the user tells it to stop
control='1'
while control != '0':
    
    print("What do you want to do ?")
    control=str(input("0)quit 1)country analyse evolution 2)compare 5 countries 3)plot death tolls 4)plots recovered and tests \n"))
    
    if(control=='1'):
        try:
            country = panda.Data_info()
            country.assign_country()
            country.compare_single_table()
        except Exception as e:
            print("Remember to select a valid country")
            print(e)
    elif(control=='2'):
        try:
            country = panda.Data_info()
            country.assign_country()
            country.compare_four_countries_with_main()
        except Exception as e:
            print("Remember to select a valid country name")
            print(e)
    elif(control=='3'):
        try:
            deaths_1m = panda.Data_info.country_deaths_per1m()
            death_toll_plot(deaths_1m)
        except Exception as e:
            print("Remember to select a valid country name")
            print(e)
    elif(control=='4'):
        try:
            rec_and_tests = panda.Data_info.countries_recovered_and_tests()
            recovered_plot(rec_and_tests)
            total_tests(rec_and_tests)
        except Exception as e:
            print("Remember to select a valid country name")
            print(e)
        
print("Goodbye")