# -*- coding: utf-8 -*-
"""
Created on Sat Mar 18 21:21:44 2021

@author: shlomo bensimhon, jamin Huang
"""
import sqlalchemy 
import mysql.connector
import pandas as pd

mydb = mysql.connector.connect(
  host="localhost",
  user="newzuser",
  password="newZuser",
  database="testdb",
  auth_plugin="mysql_native_password"
)

mycursor = mydb.cursor(buffered = True)


class Data_info:
    def __init__(self):
        self.country_name = str
        self.country_data_list = []
     
    # this method lets the user assign a country to a Data_info object
    def assign_country(self):
        x = input("Pick a country \n")
        self.country_name = x
        self.country_data_list = Data_info.assign_all_tables(x)
        

    # this method stores all three tables in regard to one country into a list
    def assign_all_tables(country):
        today_data = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + country + '"'  , con=mydb)
        yesterday_data = pd.read_sql('SELECT * FROM yesterdays_table where NAME = ' + '"' + country + '"'  , con=mydb)
        yesterday2_data = pd.read_sql('SELECT * FROM two_days_table where NAME = ' + '"' + country + '"'  , con=mydb)
        return [today_data, yesterday_data, yesterday2_data] 


    # this method lets the user return todays data for a country they desire 
    def choose_country(country):
        today_data = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + country + '"'  , con=mydb)
        return today_data

    
    # this method compares total cases, total deaths, and total recovered for the country in the Data_info class in a span of three days
    def compare_single_table(self):
    # compare total deaths, cases, etc  
        print("difference in total cases:", (int(self.country_data_list[0].TOTAL_CASES) - int(self.country_data_list[2].TOTAL_CASES)) )    
        print("difference in total deaths:", (int(self.country_data_list[0].TOTAL_DEATH) - int(self.country_data_list[2].TOTAL_DEATH)) ) 
        print("difference in total recovered:", (int(self.country_data_list[0].TOTAL_RECOVERED) - int(self.country_data_list[2].TOTAL_RECOVERED)))
        
        
    # this method compares total cases, total deaths, and total recovered for one country in three days
    def compare_tables(country): 
        # print("difference in total cases:" )
        total_cases = (int(country[0].TOTAL_CASES) - int(country[2].TOTAL_CASES))    
        # print("difference in total deaths:" )
        total_deaths = (int(country[0].TOTAL_DEATH) - int(country[2].TOTAL_DEATH))
        # print("difference in total recovered:")
        total_recovered = (int(country[0].TOTAL_RECOVERED) - int(country[2].TOTAL_RECOVERED))
        return [total_cases, total_deaths, total_recovered]
        
    # this method asks for 4 countries and compares key information about them, then it prints and returns them
    def compare_four_countries():
        print("Please Enter Country Names")
        c1 = input("Pick country 1 \n")
        c2 = input("Pick country 2 \n")
        c3 = input("Pick country 3 \n")
        c4 = input("Pick country 4 \n")
        print("======================")
        
        c1_assigned = Data_info.assign_all_tables(c1)
        c2_assigned = Data_info.assign_all_tables(c2)
        c3_assigned = Data_info.assign_all_tables(c3)
        c4_assigned = Data_info.assign_all_tables(c4)
        
        c1_compared = Data_info.compare_tables(c1_assigned)
        c2_compared = Data_info.compare_tables(c2_assigned)
        c3_compared = Data_info.compare_tables(c3_assigned)
        c4_compared = Data_info.compare_tables(c4_assigned)
        
        print(c1, "Had a", c1_compared[0], "Case Number Increase. A Total Death Increase of", c1_compared[1], ", And A Total Recovered Increase Of", c1_compared[2], "In The Last 3 Days")
        print(c2, "Had a", c2_compared[0], "Case Number Increase. A Total Death Increase of", c2_compared[1], ", And A Total Recovered Increase Of", c2_compared[2], "In The Last 3 Days")
        print(c3, "Had a", c3_compared[0], "Case Number Increase. A Total Death Increase of", c3_compared[1], ", And A Total Recovered Increase Of", c3_compared[2], "In The Last 3 Days")
        print(c4, "Had a", c4_compared[0], "Case Number Increase, A Total Death Increase of", c4_compared[1], ", Aand A Total Recovered Increase Of", c3_compared[2], "In The Last 3 Days")
        
        return [[c1, c2, c3, c4],[c1_compared, c2_compared, c3_compared, c4_compared]]
    

    # this method compares 4 countries with the main country in the Data_info Class
    def compare_four_countries_with_main(self):
            original_country_stats = Data_info.compare_tables(self.country_data_list)
            other_countries = Data_info.compare_four_countries()
            
            print(self.country_name, "Had a", original_country_stats[0], "Case Number Increase. A Total Death Increase of", original_country_stats[1], ", And A Total Recovered Increase Of", original_country_stats[2], "In The Last 3 Days")

            
            print(other_countries[0][0], "Had a", other_countries[0][0][0], "Case Number Increase. A Total Death Increase of", other_countries[1][0][1], ", And A Total Recovered Increase Of", other_countries[1][0][2], "In The Last 3 Days")

            
            print("==============================")
            
            print(other_countries[0][1], "Had a", other_countries[1][1][0], "Case Number Increase. A Total Death Increase of", other_countries[1][1][1], ", And A Total Recovered Increase Of", other_countries[1][1][2], "In The Last 3 Days")
            

            
            print("==============================")
            
            print(other_countries[0][2], "Had a", other_countries[1][2][0], "Case Number Increase. A Total Death Increase of", other_countries[1][2][1], ", And A Total Recovered Increase Of", other_countries[1][2][2], "In The Last 3 Days")

            
            
            print("==============================")
            
            print(other_countries[0][3], "Had a", other_countries[1][3][0], "Case Number Increase. A Total Death Increase of", other_countries[1][3][1], ", And A Total Recovered Increase Of", other_countries[1][3][2], "In The Last 3 Days")
        
            
            
    # this method asks for five countries and selects their deaths per 1m and returns them
    def country_deaths_per1m():
            c1 = input("Pick country 1 \n")
            c2 = input("Pick country 2 \n")
            c3 = input("Pick country 3 \n")
            c4 = input("Pick country 4 \n")
            c5 = input("Pick country 5 \n")
            
            today_c1 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c1 + '"'  , con=mydb)
            today_c2 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c2 + '"'  , con=mydb)
            today_c3 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c3 + '"'  , con=mydb)
            today_c4 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c4 + '"'  , con=mydb)
            today_c5 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c5 + '"'  , con=mydb)
            
            return [[c1, c2, c3, c4, c5], [int(today_c1.DEATHS_1M), int(today_c2.DEATHS_1M), int(today_c3.DEATHS_1M), int(today_c4.DEATHS_1M), int(today_c5.DEATHS_1M)]]
            
    # this method asks for five countries and selects their recovered and tests and returns them
    def countries_recovered_and_tests():
            c1 = input("Pick country 1 \n")
            c2 = input("Pick country 2 \n")
            c3 = input("Pick country 3 \n")
            c4 = input("Pick country 4 \n")
            c5 = input("Pick country 5 \n")
            
            today_c1 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c1 + '"'  , con=mydb)
            today_c2 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c2 + '"'  , con=mydb)
            today_c3 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c3 + '"'  , con=mydb)
            today_c4 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c4 + '"'  , con=mydb)
            today_c5 = pd.read_sql('SELECT * FROM today_table where NAME = ' + '"' + c5 + '"'  , con=mydb)
            
            return [[c1, c2, c3, c4, c5], [int(today_c1.TOTAL_RECOVERED), int(today_c2.TOTAL_RECOVERED), int(today_c3.TOTAL_RECOVERED), int(today_c4.TOTAL_RECOVERED), int(today_c5.TOTAL_RECOVERED)],              [int(today_c1.TOTAL_TESTS), int(today_c2.TOTAL_TESTS), int(today_c3.TOTAL_TESTS), int(today_c4.TOTAL_TESTS), int(today_c5.TOTAL_TESTS)]]
            
            
        

    
    
    
    
    


