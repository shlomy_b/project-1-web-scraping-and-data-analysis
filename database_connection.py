# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 15:16:47 2021

@author: shlomo bensimhon, jamin Huang
"""

import mysql.connector
import web

def connector():
    mydb = mysql.connector.connect(
      host="localhost",
      user="newzuser",
      password="newZuser",
      database="testdb",
      auth_plugin="mysql_native_password"
     )
    mycursor = mydb.cursor(buffered = True)

    return mycursor,mydb

class database_connects:
    def __init__(self):
        self.mycursor,self.mydb=connector()
        self.datas=web.datas("country_neighbour_dist_file.json")
    
    #everything needed to initialize the sql database and html file
    def initialize(self):
        self.datas.savefile()
        self.datas.extract_past_days()    
    
        self.mycursor.execute("DROP TABLE IF EXISTS TODAY_TABLE,YESTERDAYS_TABLE,TWO_DAYS_TABLE")
        self.add_to_database()
        self.add_today_data_table()
        self.add_yesterday_data_table() 
        self.add_yesterday2_data_table()
            
    # Adds the three empty tables to the database
    def add_to_database(self):
        self.mycursor.execute("CREATE TABLE TODAY_TABLE(COUNTRY_ID INT PRIMARY KEY, NAME VARCHAR(50), TOTAL_CASES VARCHAR(100), NEW_CASES VARCHAR(100), TOTAL_DEATH VARCHAR(100), NEW_DEATHS VARCHAR(100), TOTAL_RECOVERED VARCHAR(100), NEW_RECOVERED VARCHAR(100), ACTIVE_CASES VARCHAR(100), SERIOUS_CRITICAL VARCHAR(100), TOT_CASES_1M VARCHAR(100), DEATHS_1M VARCHAR(100), TOTAL_TESTS VARCHAR(100), TESTS_1M VARCHAR(100), POPULATION VARCHAR(100), CONTINENT VARCHAR(100))")
        self.mycursor.execute("CREATE TABLE YESTERDAYS_TABLE(COUNTRY_ID INT PRIMARY KEY, NAME VARCHAR(50), TOTAL_CASES VARCHAR(100), NEW_CASES VARCHAR(100), TOTAL_DEATH VARCHAR(100), NEW_DEATHS VARCHAR(100), TOTAL_RECOVERED VARCHAR(100), NEW_RECOVERED VARCHAR(100), ACTIVE_CASES VARCHAR(100), SERIOUS_CRITICAL VARCHAR(100), TOT_CASES_1M VARCHAR(100), DEATHS_1M VARCHAR(100), TOTAL_TESTS VARCHAR(100), TESTS_1M VARCHAR(100), POPULATION VARCHAR(100), CONTINENT VARCHAR(100))")
        self.mycursor.execute("CREATE TABLE TWO_DAYS_TABLE(COUNTRY_ID INT PRIMARY KEY, NAME VARCHAR(50), TOTAL_CASES VARCHAR(100), NEW_CASES VARCHAR(100), TOTAL_DEATH VARCHAR(100), NEW_DEATHS VARCHAR(100), TOTAL_RECOVERED VARCHAR(100), NEW_RECOVERED VARCHAR(100), ACTIVE_CASES VARCHAR(100), SERIOUS_CRITICAL VARCHAR(100), TOT_CASES_1M VARCHAR(100), DEATHS_1M VARCHAR(100), TOTAL_TESTS VARCHAR(100), TESTS_1M VARCHAR(100), POPULATION VARCHAR(100), CONTINENT VARCHAR(100))")
    
    # Adds today table data to database
    def add_today_data_table(self):
            sqltb1 = "INSERT INTO TODAY_TABLE(COUNTRY_ID, NAME, TOTAL_CASES, NEW_CASES, TOTAL_DEATH, NEW_DEATHS, TOTAL_RECOVERED, NEW_RECOVERED, ACTIVE_CASES, SERIOUS_CRITICAL, TOT_CASES_1M, DEATHS_1M, TOTAL_TESTS, TESTS_1M, POPULATION, CONTINENT) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" 
            valtb1 = self.datas.get_today()
            self.mycursor.executemany(sqltb1, valtb1) # execute locally the command 
            self.mydb.commit() # pushes the changes to MySQL 
        
    # Adds yesterdays table data to database
    def add_yesterday_data_table(self):
            sqltb2 = "INSERT INTO YESTERDAYS_TABLE(COUNTRY_ID, NAME, TOTAL_CASES, NEW_CASES, TOTAL_DEATH, NEW_DEATHS, TOTAL_RECOVERED, NEW_RECOVERED, ACTIVE_CASES, SERIOUS_CRITICAL, TOT_CASES_1M, DEATHS_1M, TOTAL_TESTS, TESTS_1M, POPULATION, CONTINENT) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" 
            valtb2 = self.datas.get_yesterday()
            self.mycursor.executemany(sqltb2, valtb2) # execute locally the command 
            self.mydb.commit() # pushes the changes to MySQL 
             
    # Adds yesterday2 table data to database
    def add_yesterday2_data_table(self):
            sqltb3 = "INSERT INTO TWO_DAYS_TABLE(COUNTRY_ID, NAME, TOTAL_CASES, NEW_CASES, TOTAL_DEATH, NEW_DEATHS, TOTAL_RECOVERED, NEW_RECOVERED, ACTIVE_CASES, SERIOUS_CRITICAL, TOT_CASES_1M, DEATHS_1M, TOTAL_TESTS, TESTS_1M, POPULATION, CONTINENT) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" 
            valtb3 = self.datas.get_yesterday_two()
            self.mycursor.executemany(sqltb3, valtb3) # execute locally the command 
            self.mydb.commit() # pushes the changes to MySQL 
    
